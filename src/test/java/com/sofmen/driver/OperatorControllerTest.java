package com.sofmen.driver;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sofmen.driver.dto.OperatorDTO;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {
		DeliveryDriverApplication.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(Lifecycle.PER_CLASS)
public class OperatorControllerTest {

	private static final Logger logger = LoggerFactory.getLogger(OperatorControllerTest.class);

	@Autowired
	private TestRestTemplate testRestTemplate;

	@LocalServerPort
	private int randomServerPort;

	@Test
	@DisplayName("Register Operator information Test method")
	public void registerOperator() throws Exception {
		logger.info("Register Operator information Test method started");

		final String url = "http://localhost:" + randomServerPort + "/delivery-service/v1/operator";
		URI uri = new URI(url);

		OperatorDTO operatorDTO = new OperatorDTO();
		operatorDTO.setName("Sofmen");
		operatorDTO.setEmail("test.operator@sofmen.com");
		operatorDTO.setPhoneNumber("+9187746958");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<OperatorDTO> request = new HttpEntity<>(operatorDTO, headers);

		ResponseEntity<String> result = null;

		try {
			result = testRestTemplate.postForEntity(uri, request, String.class);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		if (result.getBody() != null) {
			logger.info(result.getBody().toString());
		}
		assertEquals(200, result.getStatusCodeValue());

	}

}

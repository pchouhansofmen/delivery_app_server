package com.sofmen.driver;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.sofmen.driver.dto.DriverDTO;
import com.sofmen.driver.entity.Driver;
import com.sofmen.driver.entity.Operator;
import com.sofmen.driver.repository.DriverRepository;
import com.sofmen.driver.repository.OperatorRepository;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {
		DeliveryDriverApplication.class })
@TestPropertySource(locations = "classpath:application-test.properties")
@TestInstance(Lifecycle.PER_CLASS)
public class DriverControllerTest {

	private static final Logger logger = LoggerFactory.getLogger(DriverControllerTest.class);

	@Autowired
	private TestRestTemplate testRestTemplate;

	@Autowired
	private OperatorRepository operatorRepository;

	@Autowired
	private DriverRepository driverRepository;

	@LocalServerPort
	private int randomServerPort;

	private Operator operator = null;
	private Driver driver = null;

	@BeforeAll
	public void setup() {

		operator = new Operator();
		operator.setName("Sofmen1");
		operator.setEmail("test.operator1@sofmen.com");
		operator.setPhoneNumber("+9187746958");

		operatorRepository.save(operator);

		driver = new Driver();
		driver.setFirstName("Mukesh");
		driver.setLastName("Jain");
		driver.setEmail("mukesh.jain@sofmen.com");
		driver.setOperator(operator);
		driver.setPhoneNumber("+919858741205");
		driver.setLatitude(157892145.3);
		driver.setLongitude(159874561.6);
		driver.setDeleted(false);

		driverRepository.save(driver);

	}

	@Test
	@DisplayName("Register Driver information Test method")
	public void registerDriver() throws Exception {
		logger.info("Register Driver information Test method started");

		final String url = "http://localhost:" + randomServerPort + "/delivery-service/v1/driver";
		URI uri = new URI(url);

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add("operatorId", operator.getId());
		map.add("firstName", "Rajesh");
		map.add("middleName", "Kumar");
		map.add("lastName", "Sharma");
		map.add("email", "rajesh.sharmar@sofmen.com");
		map.add("phoneNumber", "+9187746958");
		map.add("latitude", 1257568.0);
		map.add("longitude", 1957568.0);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, headers);

		ResponseEntity<String> result = null;

		try {
			result = testRestTemplate.postForEntity(uri, request, String.class);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		if (result.getBody() != null) {
			logger.info(result.getBody().toString());
		}

		assertEquals(200, result.getStatusCodeValue());

	}

	@Test
	@DisplayName("Get Driver Info When Driver Not Exist Test method")
	public void getDriverInfoWhenDriverNotExist() throws Exception {
		logger.info("Get Driver Info When Driver Not Exist Test method started");

		final String url = "http://localhost:" + randomServerPort + "/delivery-service/v1/driver/35";
		URI uri = new URI(url);

		HttpHeaders headers = new HttpHeaders();

		HttpEntity<String> request = new HttpEntity<>(null, headers);
		ResponseEntity<DriverDTO> result = null;
		try {
			result = testRestTemplate.exchange(uri, HttpMethod.GET, request, DriverDTO.class);

		} catch (Exception e) {
			logger.debug(e.getMessage());
		}

		if (result.getBody() != null) {
			logger.info(result.getBody().toString());
		}

		assertEquals(400, result.getStatusCodeValue());
	}

	@Test
	@DisplayName("Get Driver Info Test method")
	public void getDriverInfo() throws Exception {
		logger.info("Get Driver Info Test method started");

		final String url = "http://localhost:" + randomServerPort + "/delivery-service/v1/driver/" + driver.getId();
		URI uri = new URI(url);

		HttpHeaders headers = new HttpHeaders();

		HttpEntity<String> request = new HttpEntity<>(null, headers);
		ResponseEntity<DriverDTO> result = null;
		try {
			result = testRestTemplate.exchange(uri, HttpMethod.GET, request, DriverDTO.class);

		} catch (Exception e) {
			logger.debug(e.getMessage());
		}

		if (result.getBody() != null) {
			logger.info(result.getBody().toString());
		}

		assertEquals(200, result.getStatusCodeValue());
	}

	@Test
	@DisplayName("Update Driver information When Driver Not Exist Test method")
	public void updateDriverWhenDriverNotExist() throws Exception {
		logger.info("Update Driver information When Driver Not Exist Test method started");

		final String url = "http://localhost:" + randomServerPort + "/delivery-service/v1/driver/35";
		URI uri = new URI(url);

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add("id", 35);
		map.add("operatorId", operator.getId());
		map.add("firstName", "Rajesh");
		map.add("middleName", "Kumar");
		map.add("lastName", "Sharma");
		map.add("email", "rajesh.sharmar@sofmen.com");
		map.add("phoneNumber", "+9187746958");
		map.add("latitude", 1257568.0);
		map.add("longitude", 1957568.0);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, headers);

		ResponseEntity<DriverDTO> result = null;

		try {
			result = testRestTemplate.exchange(uri, HttpMethod.PUT, request, DriverDTO.class);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		if (result.getBody() != null) {
			logger.info(result.getBody().toString());
		}

		assertEquals(400, result.getStatusCodeValue());

	}

	@Test
	@DisplayName("Update Driver information Test method")
	public void updateDriver() throws Exception {
		logger.info("Update Driver information Test method started");

		final String url = "http://localhost:" + randomServerPort + "/delivery-service/v1/driver/" + driver.getId();
		URI uri = new URI(url);

		MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
		map.add("id", driver.getId());
		map.add("operatorId", operator.getId());
		map.add("firstName", "Rajesh");
		map.add("middleName", "Kumar");
		map.add("lastName", "Sharma");
		map.add("email", "rajesh.sharmar@sofmen.com");
		map.add("phoneNumber", "+9187746958");
		map.add("latitude", 1257568.0);
		map.add("longitude", 1957568.0);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(map, headers);

		ResponseEntity<DriverDTO> result = null;

		try {
			result = testRestTemplate.exchange(uri, HttpMethod.PUT, request, DriverDTO.class);
		} catch (Exception e) {
			logger.info(e.getMessage());
		}

		if (result.getBody() != null) {
			logger.info(result.getBody().toString());
		}

		assertEquals(200, result.getStatusCodeValue());

	}

}

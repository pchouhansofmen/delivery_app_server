package com.sofmen.driver.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sofmen.driver.dto.JobListDTO;
import com.sofmen.driver.dto.JobRequestDTO;
import com.sofmen.driver.dto.JobResponseDTO;
import com.sofmen.driver.dto.JobStatusUpdateRequestDTO;
import com.sofmen.driver.dto.SuccessResponseDTO;
import com.sofmen.driver.service.JobService;

@RestController
@RequestMapping("v1/job")
public class JobController {

	private static final Logger logger = LoggerFactory.getLogger(JobController.class);

	@Autowired
	JobService jobService;

	@RequestMapping(method = POST)
	public ResponseEntity<SuccessResponseDTO> addJob(@RequestBody JobRequestDTO jobRequestDTO) {

		logger.info("Add Job Controller Method Started");

		jobService.addJob(jobRequestDTO);

		SuccessResponseDTO successResponseDTO = new SuccessResponseDTO();
		successResponseDTO.setMessage("Job Created sucessfully");
		return ResponseEntity.ok().body(successResponseDTO);

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{jobId}/accept")
	public ResponseEntity<SuccessResponseDTO> acceptJob(@PathVariable("jobId") Integer jobId,
			@RequestParam(name = "driverId") Integer driverId) {

		logger.info("Accept Job Controller Method Started");

		jobService.acceptJob(jobId, driverId);

		SuccessResponseDTO successResponseDTO = new SuccessResponseDTO();
		successResponseDTO.setMessage("Job Accepted sucessfully");
		return ResponseEntity.ok().body(successResponseDTO);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/jobs")
	public ResponseEntity<JobListDTO> getJobList(@RequestParam(name = "driverId", required = false) Integer driverId,
			@RequestParam(name = "status") String status) {

		logger.info("Get Job List Controller Method Started");

		return jobService.getJobList(driverId, status);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{jobId}")
	public ResponseEntity<JobResponseDTO> getJobDetails(@PathVariable("jobId") Integer jobId) {

		logger.info("Get Job Details Service Method Started");

		return jobService.getJobDetails(jobId);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{jobId}")
	public ResponseEntity<JobResponseDTO> updateJobStatus(@PathVariable("jobId") Integer jobId,
			@RequestParam(name = "driverId") Integer driverId,
			@Valid @ModelAttribute JobStatusUpdateRequestDTO jobStatusUpdateRequestDTO) {

		logger.info("Accept Job Controller Method Started");

		return jobService.updateJobStatus(jobId, driverId, jobStatusUpdateRequestDTO);
	}

}

package com.sofmen.driver.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sofmen.driver.dto.OperatorDTO;
import com.sofmen.driver.dto.SuccessResponseDTO;
import com.sofmen.driver.service.OperatorService;

@RestController
@RequestMapping("v1/operator")
public class OperatorController {

	private static final Logger logger = LoggerFactory.getLogger(OperatorController.class);

	@Autowired
	OperatorService operatorService;

	@RequestMapping(method = POST)
	public ResponseEntity<SuccessResponseDTO> registerOperator(@RequestBody OperatorDTO operatorDTO) {

		logger.info("Register Operator Controller Method Started");

		operatorService.registerOperator(operatorDTO);

		SuccessResponseDTO successResponseDTO = new SuccessResponseDTO();
		successResponseDTO.setMessage("Operator registerd sucessfully");
		return ResponseEntity.ok().body(successResponseDTO);

	}

}

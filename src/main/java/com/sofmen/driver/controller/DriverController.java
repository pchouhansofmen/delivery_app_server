package com.sofmen.driver.controller;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sofmen.driver.dto.DriverDTO;
import com.sofmen.driver.dto.SuccessResponseDTO;
import com.sofmen.driver.service.DriverService;

@RestController
@RequestMapping("v1/driver")
public class DriverController {

	private static final Logger logger = LoggerFactory.getLogger(DriverController.class);

	@Autowired
	DriverService driverService;

	@RequestMapping(method = POST)
	public ResponseEntity<SuccessResponseDTO> registerDriver(@Valid @ModelAttribute DriverDTO driverDTO) {

		logger.info("Register Driver Controller Method Started");

		driverService.registerDriver(driverDTO);

		SuccessResponseDTO successResponseDTO = new SuccessResponseDTO();
		successResponseDTO.setMessage("Driver registerd sucessfully");
		return ResponseEntity.ok().body(successResponseDTO);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/{driverId}")
	public ResponseEntity<DriverDTO> getDriver(@PathVariable("driverId") Integer driverId) {

		logger.info("Get Driver Controller Method Started");

		return driverService.getDriver(driverId);

	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{driverId}")
	public ResponseEntity<DriverDTO> updateDriver(@PathVariable("driverId") Integer driverId,
			@Valid @ModelAttribute DriverDTO driverDTO) {

		logger.info("Update Driver Controller Method Started");

		return driverService.updateDriver(driverId, driverDTO);

	}

}

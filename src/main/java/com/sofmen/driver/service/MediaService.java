package com.sofmen.driver.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.sofmen.driver.util.EnumUtil;
import com.sofmen.driver.util.ErrorConstatnt;

@Service
@Transactional
public class MediaService {

	private final Logger classLogger = LoggerFactory.getLogger(this.getClass());

	@Value(value = "${file.profile.location}")
	private String profileDirectory;

	@Value(value = "${file.jobImages.location}")
	private String jobImageDirectory;

	/**
	 * 
	 * @param type
	 * @return
	 */
	private String getDirectoryBasedOnType(String type) {

		classLogger.info("Get Media File directory From type method started");

		if (type.equals(EnumUtil.MediaType.DRIVER_PROFILE.typeName)) {
			return profileDirectory;
		} else {
			return jobImageDirectory;
		}

	}

	/**
	 * 
	 * @param ImageName
	 */
	public void deleteImageIfExist(String ImageName, String type) {

		classLogger.info("Store Media File method started");

		String directory = getDirectoryBasedOnType(type);

		File file = new File(directory + ImageName);
		if (file.exists()) {
			file.delete();
		}

	}

	/**
	 * 
	 * @param ImageName
	 * @param data
	 */
	public void retiveImageFromDatabase(String ImageName, byte[] data, String Type) {

		classLogger.info("Store Media File method started");

		String directory = getDirectoryBasedOnType(Type);

		File file = new File(directory + ImageName);

		if (!file.exists()) {
			try {
				File file1 = new File(directory);
				if (!file1.exists()) {
					file1.mkdir();
					file.createNewFile();
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try (BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(file))) {
				stream.write(data);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public byte[] storeFile(MultipartFile file) {
		// Normalize file name
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());

		try {
			// Check if the file's name contains invalid characters
			if (fileName.contains("..")) {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND,
						ErrorConstatnt.fileStorageError.INVALID_PATH_EXCEPTION.errorName, null);
			}

			return file.getBytes();
		} catch (IOException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					ErrorConstatnt.fileStorageError.COULD_NOT_STORE_FILE_EXCEPTION.errorName, null);
		}
	}

}

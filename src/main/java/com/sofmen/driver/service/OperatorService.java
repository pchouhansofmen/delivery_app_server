package com.sofmen.driver.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.sofmen.driver.dto.OperatorDTO;
import com.sofmen.driver.entity.Operator;
import com.sofmen.driver.repository.OperatorRepository;
import com.sofmen.driver.util.ErrorConstatnt;

@Service
@Transactional
public class OperatorService {

	private final Logger classLogger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	OperatorRepository operatorRepository;

	public void registerOperator(OperatorDTO operatorDTO) {

		classLogger.info("Register Operator Service Method Started");

		if (operatorDTO.getEmail() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.EMAIL_CAN_NOT_BE_NULL.errorName);
		}

		Operator operator = operatorRepository.findByEmailAndDeletedFalse(operatorDTO.getEmail());

		if (operator != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.OPERATOR_ALREADY_EXIST_BY_THIS_NAME.errorName);
		}

		operator = new Operator();
		operator = DtoToEntity(operator, operatorDTO);
		operator.setDeleted(false);

		operatorRepository.save(operator);

	}

	private Operator DtoToEntity(Operator operator, OperatorDTO operatorDTO) {

		classLogger.info("Operator Dto To Operator Entity Service Method Started");

		if (operatorDTO.getId() != null) {
			operator.setId(operatorDTO.getId());
		}

		if (operatorDTO.getDeleted() != null) {
			operator.setDeleted(operatorDTO.getDeleted());
		}

		if (operatorDTO.getEmail() != null) {
			operator.setEmail(operatorDTO.getEmail());
		}

		if (operatorDTO.getPhoneNumber() != null) {
			operator.setPhoneNumber(operatorDTO.getPhoneNumber());
		}

		if (operatorDTO.getName() != null) {
			operator.setName(operatorDTO.getName());
		}

		return operator;
	}

}

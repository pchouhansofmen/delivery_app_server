package com.sofmen.driver.service;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.sofmen.driver.dto.JobLineItemDTO;
import com.sofmen.driver.dto.JobListDTO;
import com.sofmen.driver.dto.JobRequestDTO;
import com.sofmen.driver.dto.JobResponseDTO;
import com.sofmen.driver.dto.JobStatusUpdateRequestDTO;
import com.sofmen.driver.entity.Driver;
import com.sofmen.driver.entity.Job;
import com.sofmen.driver.entity.JobLineItem;
import com.sofmen.driver.entity.Operator;
import com.sofmen.driver.repository.DriverRepository;
import com.sofmen.driver.repository.JobLineItemRepository;
import com.sofmen.driver.repository.JobRepository;
import com.sofmen.driver.repository.OperatorRepository;
import com.sofmen.driver.util.EnumUtil;
import com.sofmen.driver.util.ErrorConstatnt;

@Service
@Transactional
public class JobService {

	private final Logger classLogger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	DriverRepository driverRepository;

	@Autowired
	OperatorRepository operatorRepository;

	@Autowired
	JobRepository jobRepository;

	@Autowired
	JobLineItemRepository jobLineItemRepository;

	@Autowired
	MediaService mediaService;

	@Value(value = "${file.baseUrl}")
	private String baseUrl;

	public void addJob(JobRequestDTO jobRequestDTO) {

		classLogger.info("Add Job Service Method Started");

		Operator operator = new Operator();

		Job job = new Job();

		if (jobRequestDTO.getOperatorId() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.OPERATOR_CAN_NOT_BE_NULL.errorName);
		} else {
			operator = operatorRepository.findByIdAndDeletedFalse(jobRequestDTO.getOperatorId());

			if (operator == null) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						ErrorConstatnt.error.NO_OPERATOR_FOUND_FOR_THIS_OPERATOR_ID.errorName);
			} else {
				job.setOperator(operator);
			}
		}

		job = requestDtoToJobEntity(job, jobRequestDTO);

		if ((jobRequestDTO.getJobLineItems() == null) || (!(jobRequestDTO.getJobLineItems().size() > 0))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.JOB_ITEM_LIST_CAN_NOT_BE_EMPTY.errorName);
		}

		job.setDeleted(false);
		job.setStatus(EnumUtil.JobStatus.unassigned);

		job = jobRepository.save(job);

		Double totalAmount = (double) 0;
		Double totalTaxAmount = (double) 0;

		for (JobLineItemDTO jobLineItemDTO : jobRequestDTO.getJobLineItems()) {

			JobLineItem jobLineItem = new JobLineItem();
			jobLineItem = DtoToJobLineItemEntity(jobLineItem, jobLineItemDTO);
			jobLineItem.setJob(job);
			jobLineItem.setOperator(operator);
			jobLineItemRepository.save(jobLineItem);
			totalTaxAmount = totalTaxAmount + (jobLineItemDTO.getTaxAmount() * jobLineItemDTO.getQuantity());
			totalAmount = totalAmount + (jobLineItemDTO.getUnitPrice() * jobLineItemDTO.getQuantity());
		}

		job.setSubTotal(totalAmount + totalTaxAmount);
		job.setTotal(totalAmount);
		job.setTaxAmount(totalTaxAmount);

		jobRepository.save(job);

	}

	private JobLineItem DtoToJobLineItemEntity(JobLineItem jobLineItem, JobLineItemDTO jobLineItemDTO) {

		classLogger.info("Dto To Job Line Item Entity Service Method Started");

		if (jobLineItemDTO.getId() != null) {
			jobLineItem.setId(jobLineItemDTO.getId());
		}

		if (jobLineItemDTO.getName() != null) {
			jobLineItem.setName(jobLineItemDTO.getName());
		}

		if (jobLineItemDTO.getDescription() != null) {
			jobLineItem.setDescription(jobLineItemDTO.getDescription());
		}

		if (jobLineItemDTO.getExtendedPrice() != null) {
			jobLineItem.setExtendedPrice(jobLineItemDTO.getExtendedPrice());
		}

		if (jobLineItemDTO.getQuantity() != null) {
			jobLineItem.setQuantity(jobLineItemDTO.getQuantity());
		}

		if (jobLineItemDTO.getTaxAmount() != null) {
			jobLineItem.setTaxAmount(jobLineItemDTO.getTaxAmount());
		}

		if (jobLineItemDTO.getUnitPrice() != null) {
			jobLineItem.setUnitPrice(jobLineItemDTO.getUnitPrice());
		}

		return jobLineItem;
	}

	private Job requestDtoToJobEntity(Job job, JobRequestDTO jobRequestDTO) {

		classLogger.info("Request Dto To Job Entity Service Method Started");

		if (jobRequestDTO.getDescription() != null) {
			job.setDescription(jobRequestDTO.getDescription());
		}

		if (jobRequestDTO.getExternalOrderId() != null) {
			job.setExternalOrderId(jobRequestDTO.getExternalOrderId());
		}

		if (jobRequestDTO.getRecipientAdd1() != null) {
			job.setRecipientAdd1(jobRequestDTO.getRecipientAdd1());
		}

		if (jobRequestDTO.getRecipientAdd2() != null) {
			job.setRecipientAdd2(jobRequestDTO.getRecipientAdd2());
		}

		if (jobRequestDTO.getRecipientAdd3() != null) {
			job.setRecipientAdd3(jobRequestDTO.getRecipientAdd3());
		}

		if (jobRequestDTO.getRecipientAdd4() != null) {
			job.setRecipientAdd4(jobRequestDTO.getRecipientAdd4());
		}

		if (jobRequestDTO.getRecipientCity() != null) {
			job.setRecipientCity(jobRequestDTO.getRecipientCity());
		}

		if (jobRequestDTO.getRecipientLatitude() != null) {
			job.setRecipientLatitude(jobRequestDTO.getRecipientLatitude());
		}

		if (jobRequestDTO.getRecipientLongitude() != null) {
			job.setRecipientLongitude(jobRequestDTO.getRecipientLongitude());
		}

		if (jobRequestDTO.getRecipientPostalCode() != null) {
			job.setRecipientPostalCode(jobRequestDTO.getRecipientPostalCode());
		}

		if (jobRequestDTO.getSenderAdd1() != null) {
			job.setSenderAdd1(jobRequestDTO.getSenderAdd1());
		}

		if (jobRequestDTO.getSenderAdd2() != null) {
			job.setSenderAdd2(jobRequestDTO.getSenderAdd2());
		}

		if (jobRequestDTO.getSenderAdd3() != null) {
			job.setSenderAdd3(jobRequestDTO.getSenderAdd3());
		}

		if (jobRequestDTO.getSenderAdd4() != null) {
			job.setSenderAdd4(jobRequestDTO.getSenderAdd4());
		}

		if (jobRequestDTO.getSenderCity() != null) {
			job.setSenderCity(jobRequestDTO.getSenderCity());
		}

		if (jobRequestDTO.getSenderLatitude() != null) {
			job.setSenderLatitude(jobRequestDTO.getSenderLatitude());
		}

		if (jobRequestDTO.getSenderLongitude() != null) {
			job.setSenderLongitude(jobRequestDTO.getSenderLongitude());
		}

		if (jobRequestDTO.getSenderPostalCode() != null) {
			job.setSenderPostalCode(jobRequestDTO.getSenderPostalCode());
		}

		return job;
	}

	@SuppressWarnings("unlikely-arg-type")
	// @Async
	public void acceptJob(Integer jobId, Integer driverId) {

		classLogger.info("Accept Job Service Method Started");

		Job job = jobRepository.findByIdAndDeletedFalse(jobId);

		if (job == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.JOB_NOT_FOUND_FOR_THIS_ID.errorName);
		}

		Driver driver = driverRepository.findByIdAndDeletedFalse(driverId);

		if (driver == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorConstatnt.error.DRIVER_NOT_FOUND.errorName);
		}

		if (!job.getStatus().equals(EnumUtil.JobStatus.unassigned.name())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.JOB_ALREADY_ASSIGN_TO_OTHER_DRIVER.errorName);
		}

		if (job.getOperator().getId() != driver.getOperator().getId()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.OPERATOR_NOT_MATCHED_FROM_DRIVER_AND_JOB.errorName);
		}

		job.setDriver(driver);
		job.setStatus(EnumUtil.JobStatus.assigned);

		jobRepository.save(job);

	}

	public ResponseEntity<JobListDTO> getJobList(Integer driverId, String status) {

		classLogger.info("Get Job List Service Method Started");

		Driver driver = driverRepository.findByIdAndDeletedFalse(driverId);

		if (driver == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorConstatnt.error.DRIVER_NOT_FOUND.errorName);
		}

		if ((!EnumUtil.JobStatus.unassigned.name().equals(status))
				&& (!EnumUtil.JobStatus.assigned.name().equals(status))
				&& (!EnumUtil.JobStatus.picked.name().equals(status))
				&& (!EnumUtil.JobStatus.delivered.name().equals(status))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.NOT_VALID_JOB_STATUS.errorName);
		}

		List<Job> jobList = jobRepository.findByOperatorIdAndDeletedFalseAndStatus(driver.getOperator().getId(),
				status);

		JobListDTO jobListDTO = new JobListDTO();

		List<JobResponseDTO> jobs = new ArrayList<JobResponseDTO>();

		if ((jobList != null) && (jobList.size() > 0)) {
			for (Job job : jobList) {
				JobResponseDTO jobResponseDTO = new JobResponseDTO();

				jobResponseDTO = EntityToDTO(job, jobResponseDTO);

				jobs.add(jobResponseDTO);
			}
		}

		jobListDTO.setJobs(jobs);

		return ResponseEntity.ok(jobListDTO);
	}

	private JobResponseDTO EntityToDTO(Job job, JobResponseDTO jobResponseDTO) {

		classLogger.info("Job Entity To JobResponseDTO Service Method Started");

		if (job.getId() != null) {
			jobResponseDTO.setId(job.getId());
		}

		if (job.getDescription() != null) {
			jobResponseDTO.setDescription(job.getDescription());
		}

		if (job.getRecipientAdd1() != null) {
			jobResponseDTO.setRecipientAdd1(job.getRecipientAdd1());
		}

		if (job.getRecipientAdd2() != null) {
			jobResponseDTO.setRecipientAdd2(job.getRecipientAdd2());
		}

		if (job.getRecipientAdd3() != null) {
			jobResponseDTO.setRecipientAdd3(job.getRecipientAdd3());
		}

		if (job.getRecipientAdd4() != null) {
			jobResponseDTO.setRecipientAdd4(job.getRecipientAdd4());
		}

		if (job.getRecipientCity() != null) {
			jobResponseDTO.setRecipientCity(job.getRecipientCity());
		}

		if (job.getRecipientPostalCode() != null) {
			jobResponseDTO.setRecipientPostalCode(job.getRecipientPostalCode());
		}

		if (job.getSenderAdd1() != null) {
			jobResponseDTO.setSenderAdd1(job.getSenderAdd1());
		}

		if (job.getSenderAdd2() != null) {
			jobResponseDTO.setSenderAdd2(job.getSenderAdd2());
		}

		if (job.getSenderAdd3() != null) {
			jobResponseDTO.setSenderAdd3(job.getSenderAdd3());
		}

		if (job.getSenderAdd4() != null) {
			jobResponseDTO.setSenderAdd4(job.getSenderAdd4());
		}

		if (job.getSenderCity() != null) {
			jobResponseDTO.setSenderCity(job.getSenderCity());
		}

		if (job.getSenderPostalCode() != null) {
			jobResponseDTO.setSenderPostalCode(job.getSenderPostalCode());
		}

		if (job.getSubTotal() != null) {
			jobResponseDTO.setSubTotal(job.getSubTotal());
		}

		if (job.getTotal() != null) {
			jobResponseDTO.setTotal(job.getTotal());
		}

		if (job.getTaxAmount() != null) {
			jobResponseDTO.setTaxAmount(job.getTaxAmount());
		}

		if (job.getStatus() != null) {
			jobResponseDTO.setStatus(job.getStatus().name());
		}

		if (job.getOnPickUpImage() != null) {

			mediaService.retiveImageFromDatabase(
					EnumUtil.MediaType.ON_PICKUP_IMAGE.typeName + "_" + job.getId() + ".jpeg", job.getOnPickUpImage(),
					EnumUtil.MediaType.ON_PICKUP_IMAGE.typeName);

			jobResponseDTO.setOnPickUpImageName(baseUrl + "/resources/" + EnumUtil.MediaType.ON_PICKUP_IMAGE.typeName
					+ "_" + job.getId() + ".jpeg");
		}

		if (job.getOnDeliveredImage() != null) {

			mediaService.retiveImageFromDatabase(
					EnumUtil.MediaType.ON_DELIVERED_IMAGE.typeName + "_" + job.getId() + ".jpeg",
					job.getOnDeliveredImage(), EnumUtil.MediaType.ON_DELIVERED_IMAGE.typeName);

			jobResponseDTO.setOnDeliveredImageName(baseUrl + "/resources/"
					+ EnumUtil.MediaType.ON_DELIVERED_IMAGE.typeName + "_" + job.getId() + ".jpeg");
		}

		return jobResponseDTO;
	}

	public ResponseEntity<JobResponseDTO> getJobDetails(Integer jobId) {

		classLogger.info("Get Job Details Service Method Started");

		Job job = jobRepository.findByIdAndDeletedFalse(jobId);

		if (job == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.JOB_NOT_FOUND_FOR_THIS_ID.errorName);
		}

		JobResponseDTO jobResponseDTO = new JobResponseDTO();

		jobResponseDTO = EntityToDTO(job, jobResponseDTO);

		List<JobLineItem> jobListItems = jobLineItemRepository.findAllByJobId(jobId);

		List<JobLineItemDTO> lineItemDTOs = new ArrayList<JobLineItemDTO>();

		for (JobLineItem jobLineItem : jobListItems) {
			JobLineItemDTO jobLineItemDTO = new JobLineItemDTO();

			jobLineItemDTO.setId(jobLineItem.getId());
			jobLineItemDTO.setName(jobLineItem.getName());
			jobLineItemDTO.setDescription(jobLineItem.getDescription());
			jobLineItemDTO.setExtendedPrice(jobLineItem.getExtendedPrice());
			jobLineItemDTO.setQuantity(jobLineItem.getQuantity());
			jobLineItemDTO.setTaxAmount(jobLineItem.getTaxAmount());
			jobLineItemDTO.setUnitPrice(jobLineItem.getUnitPrice());

			lineItemDTOs.add(jobLineItemDTO);
		}

		jobResponseDTO.setJobLineItems(lineItemDTOs);

		return ResponseEntity.ok(jobResponseDTO);
	}

	@SuppressWarnings("unlikely-arg-type")
	public ResponseEntity<JobResponseDTO> updateJobStatus(Integer jobId, Integer driverId,
			@Valid JobStatusUpdateRequestDTO jobStatusUpdateRequestDTO) {

		Job job = jobRepository.findByIdAndDeletedFalse(jobId);

		if (job == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.JOB_NOT_FOUND_FOR_THIS_ID.errorName);
		}

		Driver driver = driverRepository.findByIdAndDeletedFalse(driverId);

		if (driver == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorConstatnt.error.DRIVER_NOT_FOUND.errorName);
		}

		if ((job.getStatus().equals(EnumUtil.JobStatus.unassigned.name())) || (job.getDriver() == null)) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.JOB_NOT_ACCEPTED_YET.errorName);
		} else if (job.getDriver().getId() != driverId) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.JOB_ASSIGN_TO_OTHER_DRIVER.errorName);
		} else if ((!jobStatusUpdateRequestDTO.getStatus().equals(EnumUtil.JobStatus.picked.name()))
				&& (!jobStatusUpdateRequestDTO.getStatus().equals(EnumUtil.JobStatus.delivered.name()))) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.NOT_VALID_JOB_STATUS.errorName);
		}

		if (jobStatusUpdateRequestDTO.getMediaFile() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorConstatnt.error.IMAGE_NOT_FOUND.errorName);
		} else if (jobStatusUpdateRequestDTO.getStatus().equals(EnumUtil.JobStatus.picked.name())) {
			mediaService.deleteImageIfExist(EnumUtil.MediaType.ON_PICKUP_IMAGE.typeName + "_" + job.getId() + ".jpeg",
					EnumUtil.MediaType.ON_PICKUP_IMAGE.typeName);

			job.setOnPickUpImage(mediaService.storeFile(jobStatusUpdateRequestDTO.getMediaFile()));
			job.setStatus(EnumUtil.JobStatus.picked);
		} else if (jobStatusUpdateRequestDTO.getStatus().equals(EnumUtil.JobStatus.delivered.name())) {
			mediaService.deleteImageIfExist(
					EnumUtil.MediaType.ON_DELIVERED_IMAGE.typeName + "_" + job.getId() + ".jpeg",
					EnumUtil.MediaType.ON_DELIVERED_IMAGE.typeName);

			job.setOnDeliveredImage(mediaService.storeFile(jobStatusUpdateRequestDTO.getMediaFile()));
			job.setStatus(EnumUtil.JobStatus.delivered);
		}

		jobRepository.save(job);

		return getJobDetails(jobId);
	}

}

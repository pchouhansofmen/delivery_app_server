package com.sofmen.driver.service;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import com.sofmen.driver.dto.DriverDTO;
import com.sofmen.driver.entity.Driver;
import com.sofmen.driver.entity.Operator;
import com.sofmen.driver.repository.DriverRepository;
import com.sofmen.driver.repository.OperatorRepository;
import com.sofmen.driver.util.EnumUtil;
import com.sofmen.driver.util.ErrorConstatnt;

@Service
@Transactional
public class DriverService {

	private final Logger classLogger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	DriverRepository driverRepository;

	@Autowired
	MediaService mediaService;

	@Autowired
	OperatorRepository operatorRepository;

	@Value(value = "${file.baseUrl}")
	private String baseUrl;

	public void registerDriver(DriverDTO driverDTO) {

		classLogger.info("Register Driver Sevice Method Started");

		Operator operator = new Operator();

		Driver driver = new Driver();

		if (driverDTO.getEmail() == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
					ErrorConstatnt.error.EMAIL_CAN_NOT_BE_NULL.errorName);
		} else {
			driver = driverRepository.findByEmailAndDeletedFalse(driverDTO.getEmail());
			if (driver != null) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						ErrorConstatnt.error.DRIVER_ALREADY_EXIST.errorName);
			} else {
				driver = new Driver();
			}
		}

		if (driverDTO.getOperatorId() != null) {
			operator = operatorRepository.findByIdAndDeletedFalse(driverDTO.getOperatorId());

			if (operator == null) {
				throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
						ErrorConstatnt.error.NO_OPERATOR_FOUND_FOR_THIS_OPERATOR_ID.errorName);
			} else {
				driver.setOperator(operator);
			}
		}

		driver = DtoToEntity(driver, driverDTO);

		driver.setDeleted(false);

		driverRepository.save(driver);

	}

	private Driver DtoToEntity(Driver driver, DriverDTO driverDTO) {

		classLogger.info("Driver Dto To Operator Entity Service Method Started");

		if (driverDTO.getId() != null) {
			driver.setId(driverDTO.getId());
		}

		if (driverDTO.getDeleted() != null) {
			driver.setDeleted(driverDTO.getDeleted());
		}

		if (driverDTO.getEmail() != null) {
			driver.setEmail(driverDTO.getEmail());
		}

		if (driverDTO.getFirstName() != null) {
			driver.setFirstName(driverDTO.getFirstName());
		}

		if (driverDTO.getLastName() != null) {
			driver.setLastName(driverDTO.getLastName());
		}

		if (driverDTO.getLatitude() != null) {
			driver.setLatitude(driverDTO.getLatitude());
		}

		if (driverDTO.getLongitude() != null) {
			driver.setLongitude(driverDTO.getLongitude());
		}

		if (driverDTO.getMiddleName() != null) {
			driver.setMiddleName(driverDTO.getMiddleName());
		}

		if (driverDTO.getPhoneNumber() != null) {
			driver.setPhoneNumber(driverDTO.getPhoneNumber());
		}

		if (driverDTO.getMediaFile() != null) {
			driver.setProfileImage(mediaService.storeFile(driverDTO.getMediaFile()));
		}

		return driver;
	}

	public ResponseEntity<DriverDTO> getDriver(Integer driverId) {

		classLogger.info("Get Driver Service Method Started");

		Driver driver = driverRepository.findByIdAndDeletedFalse(driverId);

		if (driver == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorConstatnt.error.DRIVER_NOT_FOUND.errorName);
		}

		return ResponseEntity.ok().body(driverEntityToDriverDTO(driver));
	}

	private DriverDTO driverEntityToDriverDTO(Driver driver) {

		classLogger.info("Driver Dto To Operator Entity Service Method Started");

		DriverDTO driverDTO = new DriverDTO();

		if (driver.getId() != null) {
			driverDTO.setId(driver.getId());
		}

		if (driver.getEmail() != null) {
			driverDTO.setEmail(driver.getEmail());
		}

		if (driver.getFirstName() != null) {
			driverDTO.setFirstName(driver.getFirstName());
		}

		if (driver.getLastName() != null) {
			driverDTO.setLastName(driver.getLastName());
		}

		if (driver.getLatitude() != null) {
			driverDTO.setLatitude(driver.getLatitude());
		}

		if (driver.getLongitude() != null) {
			driverDTO.setLongitude(driver.getLongitude());
		}

		if (driver.getMiddleName() != null) {
			driverDTO.setMiddleName(driver.getMiddleName());
		}

		if (driver.getOperator() != null) {
			driverDTO.setOperatorId(driver.getOperator().getId());
		}

		if (driver.getPhoneNumber() != null) {
			driverDTO.setPhoneNumber(driver.getPhoneNumber());
		}

		if (driver.getProfileImage() != null) {

			mediaService.retiveImageFromDatabase(
					EnumUtil.MediaType.DRIVER_PROFILE.typeName + "_" + driver.getId() + ".jpeg",
					driver.getProfileImage(), EnumUtil.MediaType.DRIVER_PROFILE.typeName);

			driverDTO.setProfileImageName(baseUrl + "/resources/" + EnumUtil.MediaType.DRIVER_PROFILE.typeName + "_"
					+ driver.getId() + ".jpeg");
		}

		driverDTO.setDeleted(driver.isDeleted());

		return driverDTO;
	}

	public ResponseEntity<DriverDTO> updateDriver(Integer driverId, @Valid DriverDTO driverDTO) {

		classLogger.info("Update Driver Service Method Started");

		Driver driver = driverRepository.findByIdAndDeletedFalse(driverId);

		if (driver == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ErrorConstatnt.error.DRIVER_NOT_FOUND.errorName);
		}

		if (driverDTO.getMediaFile() != null) {
			mediaService.deleteImageIfExist(EnumUtil.MediaType.DRIVER_PROFILE.typeName + "_" + driver.getId() + ".jpeg",
					EnumUtil.MediaType.DRIVER_PROFILE.typeName);

			driver.setProfileImage(mediaService.storeFile(driverDTO.getMediaFile()));
		}

		if (driverDTO.getFirstName() != null) {
			driver.setFirstName(driverDTO.getFirstName());
		}

		if (driverDTO.getLastName() != null) {
			driver.setLastName(driverDTO.getLastName());
		}

		if (driverDTO.getMiddleName() != null) {
			driver.setMiddleName(driverDTO.getMiddleName());
		}

		if (driverDTO.getPhoneNumber() != null) {
			driver.setPhoneNumber(driverDTO.getPhoneNumber());
		}

		driverRepository.save(driver);

		return ResponseEntity.ok().body(driverEntityToDriverDTO(driver));
	}

}

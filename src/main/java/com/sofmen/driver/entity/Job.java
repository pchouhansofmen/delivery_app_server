package com.sofmen.driver.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sofmen.driver.entity.audit.DateAudit;
import com.sofmen.driver.util.EnumUtil.JobStatus;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "job")
public class Job extends DateAudit {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "operator_id", nullable = false)
	private Operator operator;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "driver_id")
	private Driver driver;

	@Column(name = "description")
	private String description;

	@Column(name = "status", nullable = false)
	@Enumerated(EnumType.STRING)
	private JobStatus status;

	@Column(name = "sub_total")
	private Double subTotal;

	@Column(name = "tax_amount")
	private Double taxAmount;

	@Column(name = "total")
	private Double total;

	@Column(name = "deleted", nullable = false, columnDefinition = "TinyInt(1) default 0")
	private Boolean deleted;

	@Lob
	@Column(name = "on_pick_up_image")
	private byte[] onPickUpImage;

	@Lob
	@Column(name = "on_delivered_image")
	private byte[] onDeliveredImage;

	@Column(name = "external_order_id")
	private String externalOrderId;

	@Column(name = "sender_add1")
	private String senderAdd1;

	@Column(name = "sender_add2")
	private String senderAdd2;

	@Column(name = "sender_add3")
	private String senderAdd3;

	@Column(name = "sender_add4")
	private String senderAdd4;

	@Column(name = "sender_city", nullable = false)
	private String senderCity;

	@Column(name = "sender_postal_code", nullable = false)
	private String senderPostalCode;

	@Column(name = "sender_latitude")
	private Double senderLatitude;

	@Column(name = "sender_longitude")
	private Double senderLongitude;

	@Column(name = "recipient_add1")
	private String recipientAdd1;

	@Column(name = "recipient_add2")
	private String recipientAdd2;

	@Column(name = "recipient_add3")
	private String recipientAdd3;

	@Column(name = "recipient_add4")
	private String recipientAdd4;

	@Column(name = "recipient_city", nullable = false)
	private String recipientCity;

	@Column(name = "recipient_postal_code", nullable = false)
	private String recipientPostalCode;

	@Column(name = "recipient_latitude")
	private Double recipientLatitude;

	@Column(name = "recipient_longitude")
	private Double recipientLongitude;
}

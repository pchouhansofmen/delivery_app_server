package com.sofmen.driver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sofmen.driver.entity.JobLineItem;

@Repository
public interface JobLineItemRepository extends JpaRepository<JobLineItem, Integer> {

	@Query(nativeQuery = true, value = "Select * from job_line_item where job_id = :jobId")
	List<JobLineItem> findAllByJobId(Integer jobId);

}

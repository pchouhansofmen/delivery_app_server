package com.sofmen.driver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sofmen.driver.entity.Operator;

@Repository
public interface OperatorRepository extends JpaRepository<Operator, Integer> {

	@Query(nativeQuery = true, value = "Select * from operator where email = :email and deleted = false")
	Operator findByEmailAndDeletedFalse(String email);

	@Query(nativeQuery = true, value = "Select * from operator where id = :operatorId and deleted = false")
	Operator findByIdAndDeletedFalse(Integer operatorId);

}

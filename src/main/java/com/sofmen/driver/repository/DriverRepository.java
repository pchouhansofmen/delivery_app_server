package com.sofmen.driver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sofmen.driver.entity.Driver;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Integer> {

	@Query(nativeQuery = true, value = "Select * from driver where email = :email and deleted = false")
	Driver findByEmailAndDeletedFalse(String email);

	@Query(nativeQuery = true, value = "Select * from driver where id = :driverId and deleted = false")
	Driver findByIdAndDeletedFalse(Integer driverId);

}

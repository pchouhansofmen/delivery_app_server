package com.sofmen.driver.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sofmen.driver.entity.Job;

@Repository
public interface JobRepository extends JpaRepository<Job, Integer> {

	@Query(nativeQuery = true, value = "select * from job where id = :jobId and deleted = false")
	Job findByIdAndDeletedFalse(Integer jobId);

	@Query(nativeQuery = true, value = "select * from job where operator_id = :operatorId and status = :jobStatus and deleted = false")
	List<Job> findByOperatorIdAndDeletedFalseAndStatus(Integer operatorId, String jobStatus);

}

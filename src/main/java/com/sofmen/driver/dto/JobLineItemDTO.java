package com.sofmen.driver.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobLineItemDTO {

	private Integer id;
	private String name;
	private String description;
	private Integer quantity;
	private Double unitPrice;
	private Double extendedPrice;
	private Double taxAmount;

}

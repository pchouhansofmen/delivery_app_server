package com.sofmen.driver.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OperatorDTO {

	private Integer id;
	private String name;
	private String email;
	private String phoneNumber;
	private Boolean deleted;

}

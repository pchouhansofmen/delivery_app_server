package com.sofmen.driver.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobResponseDTO {

	private Integer id;
	private Integer operatorId;
	private Integer driverId;
	private String description;
	private String status;
	private Double subTotal;
	private Double taxAmount;
	private Double total;
	private String externalOrderId;
	private String onPickUpImageName;
	private String onDeliveredImageName;
	private String senderAdd1;
	private String senderAdd2;
	private String senderAdd3;
	private String senderAdd4;
	private String senderCity;
	private String senderPostalCode;
	private Double senderLatitude;
	private Double senderLongitude;
	private String recipientAdd1;
	private String recipientAdd2;
	private String recipientAdd3;
	private String recipientAdd4;
	private String recipientCity;
	private String recipientPostalCode;
	private Double recipientLatitude;
	private Double recipientLongitude;
	private List<JobLineItemDTO> jobLineItems;

}

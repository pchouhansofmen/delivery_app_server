package com.sofmen.driver.dto;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DriverDTO {

	private Integer id;
	private Integer operatorId;
	private String firstName;
	private String middleName;
	private String lastName;
	private MultipartFile mediaFile;
	private String profileImageName;
	private String email;
	private String phoneNumber;
	private Double latitude;
	private Double longitude;
	private Boolean deleted;

}

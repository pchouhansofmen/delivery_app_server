package com.sofmen.driver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeliveryDriverApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeliveryDriverApplication.class, args);
	}

}

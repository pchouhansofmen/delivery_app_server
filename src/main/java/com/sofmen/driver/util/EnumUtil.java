package com.sofmen.driver.util;

public class EnumUtil {

	public enum JobStatus {

		unassigned, assigned, picked, delivered;
	}

	public enum MediaType {

		DRIVER_PROFILE("Driver_Profile"), ON_PICKUP_IMAGE("On_Pickup_image"), ON_DELIVERED_IMAGE("On_Delivered_image");

		public final String typeName;

		MediaType(String typeName) {
			this.typeName = typeName;
		}
	}

}

package com.sofmen.driver.util;

public class ErrorConstatnt {

	public enum error {

		OPERATOR_ALREADY_EXIST_BY_THIS_NAME("Operator Already Exist By this name"),
		NO_OPERATOR_FOUND_FOR_THIS_OPERATOR_ID("No Operator Found For This Operator Id"),
		EMAIL_CAN_NOT_BE_NULL("Email Can not be null"), DRIVER_ALREADY_EXIST("Driver Already Exist"),
		OPERATOR_CAN_NOT_BE_NULL("Operator can not Be Null"),
		JOB_ITEM_LIST_CAN_NOT_BE_EMPTY("Job Item List can not be Empty"),
		JOB_NOT_FOUND_FOR_THIS_ID("Job not found for this Id"), DRIVER_NOT_FOUND("Driver Not Found"),
		OPERATOR_NOT_MATCHED_FROM_DRIVER_AND_JOB("Operator not matched from Driver and Job"),
		NOT_VALID_JOB_STATUS("Not Valid Job Status"),
		JOB_ALREADY_ASSIGN_TO_OTHER_DRIVER("Job Already Assign to other Driver"),
		JOB_NOT_ACCEPTED_YET("This job is Not Accepted By You Yet"),
		JOB_ASSIGN_TO_OTHER_DRIVER("Job is Assigned To Other Driver"), IMAGE_NOT_FOUND("Image Not Found");

		public final String errorName;

		error(String error) {
			this.errorName = error;
		}
	}

	public enum fileStorageError {

		COULD_NOT_STORE_FILE_EXCEPTION("Could not store this file"),
		INVALID_PATH_EXCEPTION("Sorry! Filename contains invalid path sequence");

		public final String errorName;

		fileStorageError(String error) {
			this.errorName = error;
		}
	}

}
